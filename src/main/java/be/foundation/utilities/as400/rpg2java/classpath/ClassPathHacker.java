/*
 * Created on 19-okt-2005
 *
 */
package be.foundation.utilities.as400.rpg2java.classpath;

/**
 * @author    : Geert Van Landeghem
 * Copyright : Foundation.be
 */
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Method;
import java.net.URI;
import java.net.URL;
import java.net.URLClassLoader;

public class ClassPathHacker {
    private static final Class[] parameters = new Class[] {URL.class};

    public static void addFile(String s) throws IOException {
        addFile(new File(s));
    }

    public static void addFile(File f) throws IOException {
        addURI(f.toURI());
    }

    public static void addURI(URI u) throws IOException {
        URLClassLoader sysloader=(URLClassLoader)ClassLoader.getSystemClassLoader();
        Class sysclass=URLClassLoader.class;
       
        try {
            Method method=sysclass.getDeclaredMethod("addURI", parameters);
            method.setAccessible(true);
            method.invoke(sysloader, u);
        }
        catch (Throwable t) {
            t.printStackTrace();
            throw new IOException("Error, could not add URI to system classloader");
        }
    }
    
    public static void main(String argv[]) {
        try {
            ClassPathHacker.addFile("D:\\itext-1.3.jar");
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("error:" + e.getMessage());
        }
        try {
            Class.forName("com.lowagie.text.Document");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            System.out.println("error:" + e.getMessage());            
        }
        System.out.println("Document loaded");
        URL url[] = ((URLClassLoader)ClassLoader.getSystemClassLoader()).getURLs();
        for (int i=0; i<url.length; i++) {
            System.out.println("url[" + i + "]=" + url[i]);
        }
    }
}
//How did we do that?
//Well a few tricks (hacks) are being used here. 
//First is the fact that the system classloader on a standard JRE is always an URLClassLoader. 
//The second, and more rude hack is the use of reflection to make the private addURL method
//accessible. This allows us to call this method and thereby adding files to the classpath
//of the systems default classloader. Simple but efficient� :-)
//
//http://chp.smartlog.dk/27495_Dynamicly_change_classpath_at_runtim.html