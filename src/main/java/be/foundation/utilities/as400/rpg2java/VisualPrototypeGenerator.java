/*
 * Created on 1-nov-2005
 *
 */
package be.foundation.utilities.as400.rpg2java;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.*;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.zip.ZipException;

import javax.swing.BoxLayout;
import javax.swing.DefaultCellEditor;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JTree;
import javax.swing.ListSelectionModel;
import javax.swing.SwingUtilities;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableColumnModel;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import javax.swing.table.TableModel;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeModel;

import be.foundation.utilities.as400.rpg2java.model.ClasspathNodeObject;
import be.foundation.utilities.as400.rpg2java.model.ClasspathNodeObjectType;
import be.foundation.utilities.as400.rpg2java.renderer.ClasspathNodeCellRenderer;
import be.foundation.utilities.as400.rpg2java.renderer.ClassInfoCellRenderer;
import be.foundation.utilities.as400.rpg2java.renderer.SelectedCheckboxRenderer;
import be.foundation.utilities.as400.rpg2java.classpath.ClassPathHacker;

/**
 * @author    : Geert Van Landeghem
 * Copyright : Foundation.be
 */
public class VisualPrototypeGenerator extends JPanel {
    public static final int DEFAULT_PANEL_HEIGHT = 30;

    private JTree classpath;
    private JTextField className;
    private JList classes;
    private JButton add;
    private JButton retrieveMethods;
    private JButton generate;
    private JTable methods;
    private JTextArea generatedCode;
    private DefaultMutableTreeNode root;
    private DefaultTreeModel dtm;
    private DefaultListModel dlm;
    private DefaultTableModel dtablem;

    public VisualPrototypeGenerator() {
        super();
        try {
            guiInit();

        } catch (Exception e) {
            System.out.println("Error loading gui: " + e.getMessage());
            e.printStackTrace();
        }
    }

    /**
     * Builds the gui
     */
    protected void guiInit() {
        JSplitPane splitPane = new JSplitPane();
        JPanel right = new JPanel();
        JPanel left = new JPanel();
        right.setLayout(new BorderLayout(10, 10));        
        splitPane.setDividerLocation(550);
        splitPane.setLeftComponent(left);
        splitPane.setRightComponent(right);
        this.setLayout(new BorderLayout());
        this.add(splitPane, BorderLayout.CENTER);
        left.setLayout(new GridLayout(2,1));
        JPanel left_top = new JPanel();
        left_top.setLayout(new GridLayout(2,1));
        // First panel
        JPanel left_top_top = new JPanel();
        left_top_top.setLayout(new BorderLayout());
        // Classpath label.
        left_top_top.add(buildLabelPanel("1) Adjust the classpath"), BorderLayout.NORTH);
        // Classpath tree.
        Vector v = new Vector();
        v.add(new ClasspathNodeObject("Analyzing classpath...",
                ClasspathNodeObjectType.TYPE_UNDEFINED));
        classpath = new JTree(v);
        JScrollPane scrollPane1 = new JScrollPane(classpath);
        scrollPane1.setPreferredSize(new Dimension(300, 140));
        left_top_top.add(scrollPane1, BorderLayout.CENTER);
        // Button get methods and constructors
        JPanel p1 = buildPanel(100, DEFAULT_PANEL_HEIGHT);
        p1.setLayout(new BorderLayout());
        final Container container = this.getParent();
        add = new JButton("Add jar/zip to classpath");
        add.addActionListener(new AddJarActionListener(container));
        p1.add(add, BorderLayout.SOUTH);
        left_top_top.add(p1, BorderLayout.SOUTH);
        left_top.add(left_top_top);
        // Select class label.
        JPanel left_top_bottom = new JPanel();
        left_top_bottom.setLayout(new BorderLayout());
        JPanel selectClass = new JPanel();
        selectClass.setLayout(new BoxLayout(selectClass, BoxLayout.Y_AXIS));
        selectClass.add(buildLabelPanel("2) Enter a classname or part of"));
        // Class name textfieldnew JScrollPane(generatedCode)
        JPanel p2 = buildPanel(100, DEFAULT_PANEL_HEIGHT);
        p2.setLayout(new BorderLayout());
        className = new JTextField();
        className.addKeyListener(new ClassNameKeyListener());
        p2.add(className);
        selectClass.add(p2);
        // Matching types label.
        selectClass.add(buildLabelPanel("3) Select a matching type"));
        left_top_bottom.add(selectClass, BorderLayout.NORTH);
        // List with corresponding classes
        JPanel p3 = buildPanel(100, 80);
        p3.setLayout(new BorderLayout());
        dlm = new DefaultListModel();
        classes = new JList(dlm);
        classes.addListSelectionListener(new ListSelectionListener() {
            public void valueChanged(ListSelectionEvent e) {
                retrieveMethods.setEnabled(true);
            }
        });
        classes.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        p3.add(new JScrollPane(classes));
        left_top_bottom.add(p3, BorderLayout.CENTER);
        // Button get methods and constructors
        JPanel p4 = buildPanel(100, DEFAULT_PANEL_HEIGHT);
        p4.setLayout(new BorderLayout());
        retrieveMethods = new JButton("Analyze class");
        p4.add(retrieveMethods);
        retrieveMethods.setEnabled(false);
        retrieveMethods.addActionListener(new RetrieveMethodsActionListener());
        left_top_bottom.add(p4, BorderLayout.SOUTH);
        left_top.add(left_top_bottom);
        // Second part.
        JPanel left_bottom = new JPanel();
        left_bottom.setLayout(new BorderLayout());
        // Select class label.
        left_bottom.add(buildLabelPanel("4) Select constants, constructors & methods"), BorderLayout.NORTH);
        // Table with methods
        JPanel p5 = buildPanel(100, 150);
        p5.setLayout(new BorderLayout());
        methods = buildClassInfoTable();
        p5.add(new JScrollPane(methods));
        left_bottom.add(p5, BorderLayout.CENTER);
        // Button generate prototypes
        JPanel generatePanel = new JPanel();
        generatePanel.setLayout(new BoxLayout(generatePanel, BoxLayout.Y_AXIS));
        JPanel p6 = buildPanel(100, DEFAULT_PANEL_HEIGHT);
        p6.setLayout(new BorderLayout());
        generate = new JButton("Generate RPG Prototypes");
        p6.add(generate);
        generate.addActionListener(new GenerateActionListener());
        generatePanel.add(p6);
        generatePanel.add(buildPanel(100, DEFAULT_PANEL_HEIGHT));
        left_bottom.add(generatePanel, BorderLayout.SOUTH);

        left.add(left_top);
        left.add(left_bottom);
        // Right panel
        // Select class label.
        right.add(buildLabelPanel("Generated RPG Prototypes"),
                BorderLayout.NORTH);
        // Text Area
        generatedCode = new JTextArea();
        Font font = new Font("Courier", Font.BOLD, 16);
        generatedCode.setFont(font);
        JScrollPane scrollPane3 = new JScrollPane(generatedCode);
        scrollPane3.setPreferredSize(new Dimension(300, 800));
        right.add(scrollPane3, BorderLayout.CENTER);
    }

    /**
     * Builds the table representing the information found in a class.
     * @return the table 
     */
    public JTable buildClassInfoTable() {
        DefaultTableColumnModel dtcm = new DefaultTableColumnModel();
        // Select column.
        TableColumn tc = new TableColumn();
        tc.setHeaderValue("Select");
        tc.setModelIndex(0);
        tc.setMinWidth(50);
        tc.setMaxWidth(50);
        JCheckBox cb = new JCheckBox();
        tc.setCellEditor(new DefaultCellEditor(cb));
        tc.setCellRenderer(new SelectedCheckboxRenderer());
        dtcm.addColumn(tc);
        // Method name
        tc = new TableColumn();
        tc.setHeaderValue("Constant/Constructor/Method");
        tc.setModelIndex(1);
        tc.setMinWidth(150);
        tc.setCellRenderer(new ClassInfoCellRenderer());
        dtcm.addColumn(tc);
        // Methods
        tc = new TableColumn();
        tc.setHeaderValue("Method");
        tc.setModelIndex(2);
        tc.setMinWidth(0);
        tc.setMaxWidth(0);
        tc.setCellRenderer(new ClassInfoCellRenderer());
        dtcm.addColumn(tc);
        // Hidden type (constructor/method)
        tc = new TableColumn();
        tc.setMinWidth(0);
        tc.setMaxWidth(0);
        tc.setModelIndex(3);
        tc.setCellRenderer(new ClassInfoCellRenderer());
        dtcm.addColumn(tc);
        dtablem = new DefaultTableModel(0, 4);
        JTable table = new JTable();
        table.setModel(dtablem);
        table.setColumnModel(dtcm);
        return table;
    }

    /**
     * Builds a default panel containing only a label.
     * 
     * @param label the label to add. 
     * @return the build panel.
     */
    public JPanel buildLabelPanel(String label) {
        JPanel panel = buildPanel(100, DEFAULT_PANEL_HEIGHT);

        JLabel jLabel = new JLabel(label);
        jLabel.setOpaque(true);
        jLabel.setBackground(Color.red);
        panel.add(jLabel);
        return panel;
    }

    /**
     * Builds a default panel.
     * 
     * @param width the preferred width of the panel
     * @param height the preferred height of the panel
     * @return the build panel.
     */
    private JPanel buildPanel(int width, int height) {
        JPanel panel = new JPanel();
        panel.setLayout(new FlowLayout(FlowLayout.LEFT));
        panel.setPreferredSize(new Dimension(width, height));
        return panel;
    }

    /**
     * Analyzes the classpath. Each jarfile found on the classpath will
     * be analyzed and a visual representation of the classpath is rendered
     * as a tree
     */
    public void analyzeClasspath() {
        URL url[] = ((URLClassLoader) ClassLoader.getSystemClassLoader())
                .getURLs();
        root = new DefaultMutableTreeNode(new ClasspathNodeObject("CLASSPATH",
                ClasspathNodeObjectType.TYPE_JAVA));
        String sun = System.getProperty("sun.boot.class.path");
        String os = System.getProperty("os.name");
        String token = "";
        if (os.startsWith("Windows")) {
            token = ";";
        } else {
            token = ":";
        }
        StringTokenizer tk = new StringTokenizer(sun, token);
        while (tk.hasMoreTokens()) {
            String jar = tk.nextToken();
            try {
                root.add(analyzeJar(new URL("file://"
                        + new File(jar).getAbsolutePath())));
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
        }

        for (int i = 0; i < url.length; i++) {
            URL entry = url[i];
            root.add(analyzeJar(entry));
        }
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                dtm = new DefaultTreeModel(root);
                classpath.setModel(dtm);
            }
        });
    }

    /**
     * Analyzes the jar file passed as an url. All packages will be 
     * added as node with the contained classes as leaves.
     * 
     * @param url the url to the jar file
     * @return the treenode with a visual presentation of the jarfile.
     */
    public DefaultMutableTreeNode analyzeJar(URL url) {
        ClasspathNodeObject nodeObject = new ClasspathNodeObject(
                url.toString(), ClasspathNodeObjectType.TYPE_JAR);
        DefaultMutableTreeNode node = new DefaultMutableTreeNode(nodeObject);

        // Jar/zip entries?
        try {
            JarFile jarFile = new JarFile(url.toString().substring(5));
            Enumeration en = jarFile.entries();
            String lastPackage = "";
            DefaultMutableTreeNode lastPackageNode = null;
            while (en.hasMoreElements()) {
                JarEntry jarEntry = (JarEntry) en.nextElement();
                System.out.println("jarEntry" + jarEntry.toString());
                if (jarEntry.toString().endsWith(".class")) {
                    String entryName = jarEntry.getName();
                    entryName = entryName.substring(0, entryName
                            .indexOf(".class"));
                    StringBuffer sb = new StringBuffer();
                    for (int i = 0; i < entryName.length(); i++) {
                        char c = entryName.charAt(i);
                        if (c == '/') {
                            sb.append('.');
                        } else {
                            sb.append(c);
                        }
                    }
                    entryName = sb.toString();
                    System.out.println("entryName:" + entryName);
                    int last = entryName.lastIndexOf(".");
                    String name = null;
                    String packageName = null;
                    if (last > -1) {
                        name = entryName.substring(last + 1);
                        packageName = entryName.substring(0, last);
                    } else {
                        packageName = "default";
                        name = entryName;
                    }

                    if (lastPackage.equals(packageName)) {
                        nodeObject = new ClasspathNodeObject(name,
                                ClasspathNodeObjectType.TYPE_JAVA);
                        DefaultMutableTreeNode jarEntryNode = new DefaultMutableTreeNode(
                                nodeObject);
                        lastPackageNode.add(jarEntryNode);
                    } else {
                        nodeObject = new ClasspathNodeObject(packageName,
                                ClasspathNodeObjectType.TYPE_PACKAGE);
                        lastPackageNode = new DefaultMutableTreeNode(nodeObject);
                        node.add(lastPackageNode);
                        nodeObject = new ClasspathNodeObject(name,
                                ClasspathNodeObjectType.TYPE_JAVA);
                        DefaultMutableTreeNode jarEntryNode = new DefaultMutableTreeNode(
                                nodeObject);
                        lastPackageNode.add(jarEntryNode);
                        lastPackage = packageName;
                    }
                }
            }
        } catch(ZipException ze) {
            System.out.println("Could not open zipfile:" + url.toString()); 
        } catch (IOException e) {
            e.printStackTrace();
        }
        return node;
    }

    /**
     * Initializes the classpath tree.
     */
    public void initialize() {
        analyzeClasspath();
        classpath.setCellRenderer(new ClasspathNodeCellRenderer());
    }

    /**
     * Traverse a JTree using a prefix and return the names of those 
     * classes which name start with the prefix
     * 
     * @param tree the JTree
     * @param prefix the classname prefix
     * @return the list of classes
     */
    public List traverse(JTree tree, String prefix) {
        List list = new ArrayList();
        TreeModel model = tree.getModel();
        if (model != null) {
            Object root = model.getRoot();
            list = walk(model, root, prefix, dlm);
        }
        return list;
    }

    /**
     * Walk through a tree model and return the names of those classes
     * which name start with the prefix
     * @param model the tree model
     * @param o the node object to use
     * @param prefix the classname prefix
     * @param listModel the list model to check for doubles
     * @return
     */
    protected List walk(TreeModel model, Object o, String prefix,
            DefaultListModel listModel) {
        List list = new ArrayList();
        int cc = model.getChildCount(o);
        for (int i = 0; i < cc; i++) {
            final Object child = model.getChild(o, i);
            final DefaultMutableTreeNode childNode = (DefaultMutableTreeNode) child;
            ((DefaultMutableTreeNode) child).getParent().toString();
            if (model.isLeaf(child)) {
                // Child leave to check
                final String childStr = child.toString();
                if (childStr.startsWith(prefix)) {
                    String node = childStr
                            + " - "
                            + ((DefaultMutableTreeNode) child).getParent()
                                    .toString();
                    if (!list.contains(node)) {
                        list.add(node);
                    }
                }
            } else {
                list.addAll(walk(model, child, prefix, listModel));
            }
        }
        return list;
    }

    /**
     * @param args
     */
    public static void main(String[] args) {
        JFrame frame = new JFrame(
                "VisualPrototypeGenerator (Open Source@foundation.be)");
        VisualPrototypeGenerator vpg = new VisualPrototypeGenerator();
        frame.setContentPane(vpg);
        Dimension d = Toolkit.getDefaultToolkit().getScreenSize();
        
        frame.setSize(d);
        frame.setVisible(true);
        frame.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent we) {
                System.exit(0);
            }
        });
        vpg.initialize();
    }
    
    /**
     * Executes code when the generate button is clicked.
     * @author gvl
     */
    class GenerateActionListener implements ActionListener {
        public void actionPerformed(ActionEvent ae) {
            generatedCode.setText("");
            StringBuffer sb = new StringBuffer();
            TableModel tm = methods.getModel();
            for (int i = 0; i < tm.getRowCount(); i++) {
                Boolean b = (Boolean) tm.getValueAt(i, 0);
                if (b.booleanValue()) {
                    String className = "";
                    String methodName = "";
                    Class[] parameterTypes = null;
                    Object value = tm.getValueAt(i, 2);
                    if (value instanceof Method ||
                        value instanceof Constructor) {
                        if (value instanceof Method) {
                            Method m = (Method) value;
                            className = m.getDeclaringClass().getCanonicalName();
                            methodName = m.getName();
                            parameterTypes = m.getParameterTypes();
                        } else if (value instanceof Constructor) {
                            Constructor c = (Constructor) value;
                            className = c.getDeclaringClass()
                                    .getCanonicalName();
                            methodName = c.getDeclaringClass().getSimpleName();
                            parameterTypes = c.getParameterTypes();
                        }
                        try {
                            sb.append(
                                    PrototypeGenerator.generateRPG2JavaPrototypes(className,
                                            methodName, parameterTypes));
                        } catch (ClassNotFoundException e) {
                            e.printStackTrace();
                        } catch (NoSuchMethodException e) {
                            e.printStackTrace();
                        }
                    } else if (value instanceof Field) {
                        Field field = (Field) value;
                        className = field.getDeclaringClass().getCanonicalName();                        
                        try {
                            sb.append(
                                    PrototypeGenerator.generateRPG2JavaConstants(className, field.getName()));
                        } catch (SecurityException e) {
                            e.printStackTrace();
                        } catch (ClassNotFoundException e) {
                            e.printStackTrace();
                        } catch (NoSuchFieldException e) {
                            e.printStackTrace();
                        }                            
                    }
                }
            }
            generatedCode.setText(sb.toString());
        }
    }

    /**
     * Executes code when the retrieve methods button is clicked.
     * @author gvl
     */
    class RetrieveMethodsActionListener implements ActionListener {
        public void actionPerformed(ActionEvent ae) {
            dtablem.setNumRows(0);
            String selected = classes.getSelectedValue().toString();
            String className = selected
                    .substring(selected.indexOf("-") + 2)
                    + "."
                    + selected.substring(0, selected.indexOf("-") - 1);
            List methods = null;
            List constructors = null;
            List fields = null;
            try {
                constructors = PrototypeGenerator.getJavaConstructors(className);
                methods = PrototypeGenerator.getJavaMethods(className);
                fields = PrototypeGenerator.getJavaConstants(className);
                Collections.sort(constructors, new Comparator() {
                    public int compare(Object o1, Object o2) {
                        if (o1 instanceof Constructor
                                && o2 instanceof Constructor) {
                            Constructor c1 = (Constructor) o1;
                            Constructor c2 = (Constructor) o2;
                            return c1.getName().compareTo(c2.getName());
                        }
                        return 0;
                    }
                });
                Collections.sort(methods, new Comparator() {
                    public int compare(Object o1, Object o2) {
                        if (o1 instanceof Method && o2 instanceof Method) {
                            Method c1 = (Method) o1;
                            Method c2 = (Method) o2;
                            return c1.getName().compareTo(c2.getName());
                        }
                        return 0;
                    }
                });
                Collections.sort(fields, new Comparator() {
                    public int compare(Object o1, Object o2) {
                        if (o1 instanceof Field && o2 instanceof Field) {
                            Field c1 = (Field) o1;
                            Field c2 = (Field) o2;
                            return c1.getName().compareTo(c2.getName());
                        }
                        return 0;
                    }
                    
                });
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
            // Add constants, constructors and methods.
            if (fields != null) {
                Iterator it = fields.iterator();
                while (it.hasNext()) {
                    Field field = (Field) it.next();
                    Vector row = new Vector();
                    row.add(new Boolean(false));
                    row.add(field.toString());
                    row.add(field);
                    row.add("field");
                    dtablem.addRow(row);
                }
            }            
            if (constructors != null) {
                Iterator it = constructors.iterator();
                while (it.hasNext()) {
                    Constructor method = (Constructor) it.next();
                    Vector row = new Vector();
                    row.add(new Boolean(false));
                    try {
                        row.add(PrototypeGenerator.getJavaMethodAsString(
                                method.getDeclaringClass()
                                        .getCanonicalName(), method
                                        .getName(), method
                                        .getParameterTypes()));
                    } catch (ClassNotFoundException e) {
                        e.printStackTrace();
                    } catch (NoSuchMethodException e) {
                        e.printStackTrace();
                    }
                    row.add(method);
                    row.add("constructor");
                    dtablem.addRow(row);
                }
            }
            if (methods != null) {
                Iterator it = methods.iterator();
                while (it.hasNext()) {
                    Method method = (Method) it.next();
                    Vector row = new Vector();
                    row.add(new Boolean(false));
                    try {
                        row.add(PrototypeGenerator.getJavaMethodAsString(
                                method.getDeclaringClass()
                                        .getCanonicalName(), method
                                        .getName(), method
                                        .getParameterTypes()));
                    } catch (ClassNotFoundException e) {
                        e.printStackTrace();
                    } catch (NoSuchMethodException e) {
                        e.printStackTrace();
                    }
                    row.add(method);
                    row.add("method");
                    dtablem.addRow(row);
                }
            }
        

        }
    }

    /**
     * Executes code when a key is pressed in the class name text field.
     * @author gvl
     *
     */
    class ClassNameKeyListener implements KeyListener {
        public void keyTyped(KeyEvent e) {
        }

        public void keyPressed(KeyEvent e) {
        }

        public void keyReleased(KeyEvent e) {
            SwingUtilities.invokeLater(new Runnable() {
                public void run() {
                    dlm.removeAllElements();
                    retrieveMethods.setEnabled(false);
                }
            });
            String prefix = className.getText();
            if (!prefix.equals("")) {
                final List list = traverse(classpath, prefix);
                Collections.sort(list);
                SwingUtilities.invokeLater(new Runnable() {
                    public void run() {
                        for (int i = 0; i < list.size(); i++) {
                            dlm.addElement(list.get(i));
                        }
                    }
                });
            }

        }
    }
    
    class AddJarActionListener implements ActionListener {
        private Container container;
        public AddJarActionListener(Container container) {
            this.container = container;
        }
        public void actionPerformed(ActionEvent ae) {
            JFileChooser chooser = new JFileChooser();
            chooser.setApproveButtonText("Select");
            javax.swing.filechooser.FileFilter jarFileFilter = new javax.swing.filechooser.FileFilter() {
                public boolean accept(File pathname) {
                    if (pathname.getName().endsWith("jar")
                            || pathname.getName().endsWith("zip")
                            || pathname.isDirectory()) {
                        return true;
                    }
                    return false;
                }

                public String getDescription() {
                    return "JAR and ZIP files only";
                }
            };
            chooser.setFileFilter(jarFileFilter);
            int retValue = chooser.showOpenDialog(container);
            if (retValue == JFileChooser.APPROVE_OPTION) {
                final File selected = chooser.getSelectedFile();
                // Add file to classpath.
                try {
                    ClassPathHacker.addFile(selected);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                SwingUtilities.invokeLater(new Runnable() {
                    public void run() {
                        try {
                            dtm.insertNodeInto(
                                    analyzeJar(new URL("file://"
                                            + selected.getAbsolutePath())),
                                    root, root.getChildCount());
                        } catch (MalformedURLException e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        }
    }

    /**
     * @Author    : Geert Van Landeghem
     * @Copyright : Foundation.be
     */
    public static class PrototypeGenerator {

        /**
         * Returns the names of the constructor methods for a given class as a List.
         *
         * @param className the classname
         * @return the list of constructor methods
         * @throws ClassNotFoundException
         */
        public static List getJavaConstants(String className) throws ClassNotFoundException {
            Class clazz = null;
            try {
                clazz = Class.forName(className);
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
                throw (e);
            }
            Field f[] = clazz.getFields();
            List fields = new ArrayList();
            for (int i = 0; i < f.length; i++) {
                Field field = f[i];
                System.out.println("Field:" + field.toString());
                if (field.toString().indexOf("static") > -1) {
                    fields.add(field);
                }
            }
            return fields;
        }

        public static String generateRPG2JavaConstants(String className, String fieldName)
        throws ClassNotFoundException, SecurityException, NoSuchFieldException {
            Class clazz = null;
            Field field = null;
            try {
                clazz = Class.forName(className);
                field = clazz.getField(fieldName);
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
                throw (e);
            }
            String type = field.getType().getSimpleName();
            if (!(type.equals("byte") ||
                    type.equals("char") ||
                    type.equals("short")||
                    type.equals("int")  ||
                    type.equals("long") ||
                    type.equals("float") ||
                    type.equals("double"))) {
                return "";
            }
            // Field name
            StringBuffer sb = new StringBuffer();
            String dSpec = "     D";
            String line = "";
            String name = field.getName();
            if (name.length() > 15) {
                name += "...";
                line += dSpec + name;
                sb.append(line + "\n");
                line = dSpec;
            } else {
                line = dSpec + name;
            }
            // Field type
            line = fillLineWithType(line, type);
            line = fillLine(line, ' ', 43) + "Inz(";
            // Value.
            Object object = new Object();
            String value = "";
            try {
                value = field.get(object).toString();
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
            line += value + ")\n";
            sb.append(line);
            return sb.toString();
        }


        /**
         * Returns the names of the constructor methods for a given class as a List.
         *
         * @param className the classname
         * @return the list of constructor methods
         * @throws ClassNotFoundException
         */
        public static List getJavaConstructors(String className) throws ClassNotFoundException {
            Class clazz = null;
            try {
                clazz = Class.forName(className);
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
                throw (e);
            }
            Constructor methods[] = clazz.getConstructors();
            return Arrays.asList(methods);
        }




        /**
         * Returns the names of the methods for a given class as a List.
         *
         * @param className the classname
         * @return the list of class methods
         * @throws ClassNotFoundException
         */
        public static List getJavaMethods(String className) throws ClassNotFoundException {
            Class clazz = null;
            try {
                clazz = Class.forName(className);
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
                throw (e);
            }
            Method methods[] = clazz.getMethods();
            return Arrays.asList(methods);
        }

        /**
         * Returns the methods for a given class as a List of Strings.
         *
         * @param className the classname
         * @return the list of methods as strings
         * @throws ClassNotFoundException
         */
        public static List getJavaMethodsAsString(String className) throws ClassNotFoundException {
            List list = new ArrayList();
            Class clazz = null;
            try {
                clazz = Class.forName(className);
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
                throw (e);
            }
            Method methods[] = clazz.getMethods();
            for (int i=0; i < methods.length; i++) {
                Method m = methods[i];
                String name = m.getName();
                Class paramType[] = m.getParameterTypes();
                Class returnType = m.getReturnType();
                int modifiers = m.getModifiers();
                String method = Modifier.toString(modifiers) + " " + returnType.getSimpleName() + " " + name + "(";
                for (int y=0; y<paramType.length; y++) {
                    Class paramClass = paramType[y];
                    String paramName = paramClass.getSimpleName();
                    method = method + paramName + " p" +(y+1);
                    if (y < paramType.length -1) {
                        method = method + ", ";
                    }
                }
                method = method + ")";
                list.add(method);
            }
            return list;
        }

        /**
         * Returns the info for a given method as a formatted string.
         *
         * @param className the classname
         * @param methodName the method name
         * @param parameterTypes the parameter types
         * @return String representing method info.
         * @throws ClassNotFoundException
         * @throws NoSuchMethodException
         */
        public static String getJavaMethodAsString(String className, String methodName, Class[] parameterTypes)
            throws ClassNotFoundException, NoSuchMethodException {
            // Load class.
            Class clazz = null;
            try {
                clazz = Class.forName(className);
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
                throw (e);
            }
            // Use reflection classes to retrieve method object.
            Method m = null;
            Constructor c = null;
            // Get information about method name, parameter types and modifiers
            Class paramType[] = null;
            Class returnTypeClass = null;
            String returnTypeSimpleName = null;
            int modifiers = 0;
            try {
                if (className.equals(methodName)) {
                    c = clazz.getConstructor(parameterTypes);
                    paramType = c.getParameterTypes();
                    returnTypeClass = clazz;
                    returnTypeSimpleName = returnTypeClass.getSimpleName();
                    modifiers = c.getModifiers();
                    methodName = c.getDeclaringClass().getSimpleName();
                } else {
                    m = clazz.getDeclaredMethod(methodName, parameterTypes);
                    paramType = m.getParameterTypes();
                    returnTypeClass = m.getReturnType();
                    returnTypeSimpleName = returnTypeClass.getSimpleName();
                    modifiers = m.getModifiers();
                }
            } catch (SecurityException e) {
                e.printStackTrace();
                throw e;
            } catch (NoSuchMethodException e) {
                e.printStackTrace();
                throw e;
            }
            String method =  methodName + "(";
            for (int y=0; y<paramType.length; y++) {
                Class paramClass = paramType[y];
                String paramName = paramClass.getSimpleName();
                method = method + paramName + " p" +(y+1);
                if (y < paramType.length -1) {
                    method = method + ", ";
                }
            }
            method = method + ") - " + returnTypeSimpleName;
            return method;

        }


        /**
         * Returns the rpg prototype of a given java method.
         *
         * @param className the classname
         * @param methodName the name of the method
         * @param parameterTypes the method parameter types
         * @return
         * @throws ClassNotFoundException
         * @throws NoSuchMethodException
         */
        public static String generateRPG2JavaPrototypes(String className, String methodName, Class[] parameterTypes)
            throws ClassNotFoundException, NoSuchMethodException {
            // Load class.
            Class clazz = null;
            try {
                clazz = Class.forName(className);
            } catch (ClassNotFoundException e) {
                System.out.println("ClassNotFoundException:" + clazz.getName());
                //e.printStackTrace();
                throw (e);
            }
            // classname
            String dSpec = "     D";
            int index = className.lastIndexOf('.');
            String clazzName = "";
            if (index > -1) {
                clazzName = className.substring(index+1);
            }
            // Use reflection classes to retrieve method object.
            Method m = null;
            Constructor c = null;
            // Normal method?
            boolean isConstructor = false;
            // Get information about method name, parameter types and modifiers
            Class paramType[] = null;
            Class returnTypeClass = null;
            String returnTypeSimpleName = null;
            int modifiers = 0;
            try {
                if (clazzName.equals(methodName)) {
                    c = clazz.getConstructor(parameterTypes);
                    paramType = c.getParameterTypes();
                    returnTypeClass = clazz;
                    returnTypeSimpleName = returnTypeClass.getSimpleName();
                    modifiers = c.getModifiers();
                    isConstructor = true;
                } else {
                    m = clazz.getDeclaredMethod(methodName, parameterTypes);
                    paramType = m.getParameterTypes();
                    returnTypeClass = m.getReturnType();
                    returnTypeSimpleName = returnTypeClass.getSimpleName();
                    modifiers = m.getModifiers();
                }
            } catch (SecurityException e) {
                e.printStackTrace();
                throw e;
            } catch (NoSuchMethodException e) {
                e.printStackTrace();
                throw e;
            }
            // Append information about constructor/method to output
            StringBuffer sb = new StringBuffer();
            sb.append(dSpec).append("* ");
            sb.append(Modifier.toString(modifiers)).append(" ").append(returnTypeSimpleName);
            sb.append(" ").append(methodName).append("(");
            for (int y=0; y<paramType.length; y++) {
                Class paramClass = paramType[y];
                String paramName = paramClass.getSimpleName();
                sb.append(paramName + " p" +(y+1));
                if (y < paramType.length -1) {
                    sb.append(", ");
                }
            }
            sb.append(")");
            sb.append("\n");

            // Prototype definition construction
            // Method Name + PR
            String line = "";
            // If method name is to long, use ... and next line.
            String classAndMethod = clazzName + "$" + methodName;
            // Constructor method?
            if (isConstructor) {
                classAndMethod = clazzName + "$new" + methodName;
            }
            if (classAndMethod.length() > 15) {
                classAndMethod += "...";
                line = dSpec + classAndMethod;
                sb.append(line + "\n");
                line = dSpec;
            } else {
                line = dSpec + classAndMethod;
            }
            line = fillLine(line, ' ', 23);
            line = line + "PR";

            // ReturnType
            // Object - O or primitive?
            line = fillLineWithType(line, returnTypeSimpleName);
            boolean returnTypeIsObject = (line.substring(39, 40).equals("O"));

            // EXTPROC(*JAVA:
            line = fillLine(line, ' ', 43) + "EXTPROC(*JAVA";
            sb.append(line + "\n");

            // 'JAVA-CLASS-NAME':$
            if (className.length() >33) {
               line = fillLine(dSpec,' ', 43) + ":\'" + className.substring(0,33) + "-";
               sb.append(line + "\n");
               line = fillLine(dSpec,' ', 43) + className.substring(33) + "\'";
               sb.append(line + "\n");
            }else {
                line = fillLine(dSpec,' ', 43) + ":\'" + className + "\'";
                sb.append(line + "\n");
            }

            // 'JAVA-METHOD-NAME':)
            if (isConstructor) {
                line = fillLine(dSpec,' ', 43) + ":*CONSTRUCTOR)";
                sb.append(line + "\n");
            } else {
                line = fillLine(dSpec,' ', 43) + ":\'" + methodName + "\')";
                sb.append(line + "\n");
            }

            // Static method?
            if (Modifier.toString(modifiers).indexOf("static") > -1) {
                line = fillLine(dSpec,' ', 43) + "Static";
                sb.append(line + "\n");
            }

            // OBJECT return type?
            if (returnTypeIsObject) {
                line = fillLine(dSpec,' ', 43) + "Class(*JAVA";
                sb.append(line + "\n");
                line = fillLine(dSpec,' ', 43) + ":\'" + returnTypeClass.getCanonicalName() + "\')";
                sb.append(line + "\n");
            }

            // PARAMETERS
            if (parameterTypes != null) {
                for (int i=0; i < parameterTypes.length; i++) {
                    line = dSpec + "p" + (i+1);
                    Class parameter = parameterTypes[i];
                    String parameterSimpleName = parameter.getSimpleName();
                    // Add class description if parameter is of type object.
                    line = fillLineWithType(line, parameterSimpleName);
                    boolean parameterTypeIsObject = (line.substring(39, 40).equals("O"));
                    if (parameterTypeIsObject) {
                        line = fillLine(line,' ', 43) + "Class(*JAVA";
                        sb.append(line + "\n");
                        line = fillLine(dSpec,' ', 43) + ":\'" + parameter.getCanonicalName() + "\')";
                        sb.append(line + "\n");
                    } else {
                        sb.append(line + "\n");
                    }
                }
            }
            return sb.toString();
        }

        /**
         * Adds the rpg type of the return value of a java method.
         *
         * @param originalLine the original line with the partial rpg prototype
         * @param returnTypeName the java return type
         * @return the rpg return type
         */
        public static String fillLineWithType(String originalLine, String returnTypeName) {
            String line = "";
            if (returnTypeName.indexOf("[]") > -1) {
                if (returnTypeName.equals("boolean[]")) {
                    // RPG indicator
                    line = fillLine(originalLine, ' ', 39);
                    line = line + "N";
                } else if (returnTypeName.equals("byte[]")) {
                    line = fillLine(originalLine, ' ', 38);
                    // RPG integer
                    // line = line + "3I 0";
                    // RPG alfanumeric
                    line = line + "?A";
                } else if (returnTypeName.equals("char[]")) {
                    line = fillLine(originalLine, ' ', 38);
                    // RPG UCS-2
                    line = line + "1C";
                } else if (returnTypeName.equals("short[]")) {
                    line = fillLine(originalLine, ' ', 38);
                    // RPG 2-byte integer
                    line = line + "5I 0";
                } else if (returnTypeName.equals("int[]")) {
                    line = fillLine(originalLine, ' ', 37);
                    // RPG 4-byte integer
                    line = line + "10I 0";
                } else if (returnTypeName.equals("long[]")) {
                    line = fillLine(originalLine, ' ', 37);
                    // RPG 8-byte integer
                    line = line + "20I 0";
                } else if (returnTypeName.equals("float[]")) {
                    line = fillLine(originalLine, ' ', 38);
                    // RPG 4-byte float
                    line = line + "4F 0";
                } else if (returnTypeName.equals("double[]")) {
                    line = fillLine(originalLine, ' ', 38);
                    // RPG 8-byte float
                    line = line + "8F 0";
                } else {
                    line = fillLine(originalLine, ' ', 39);
                    line = line + "O";
                }
            } else {
                if (returnTypeName.equals("boolean")) {
                    // RPG indicator
                    line = fillLine(originalLine, ' ', 39);
                    line = line + "N";
                } else if (returnTypeName.equals("byte")) {
                    line = fillLine(originalLine, ' ', 38);
                    // RPG integer
                    // line = line + "3I 0";
                    // RPG alfanumeric
                    line = line + "1A";
                } else if (returnTypeName.equals("char")) {
                    line = fillLine(originalLine, ' ', 38);
                    // RPG UCS-2
                    line = line + "1C";
                } else if (returnTypeName.equals("short")) {
                    line = fillLine(originalLine, ' ', 38);
                    // RPG 2-byte integer
                    line = line + "5I 0";
                } else if (returnTypeName.equals("int")) {
                    line = fillLine(originalLine, ' ', 37);
                    // RPG 4-byte integer
                    line = line + "10I 0";
                } else if (returnTypeName.equals("long")) {
                    line = fillLine(originalLine, ' ', 37);
                    // RPG 8-byte integer
                    line = line + "20I 0";
                } else if (returnTypeName.equals("float")) {
                    line = fillLine(originalLine, ' ', 38);
                    // RPG 4-byte float
                    line = line + "4F 0";
                } else if (returnTypeName.equals("double")) {
                    line = fillLine(originalLine, ' ', 38);
                    // RPG 8-byte float
                    line = line + "8F 0";
                } else if (returnTypeName.equals("void")) {
                    line = fillLine(originalLine, ' ', 40);
                } else {
                    line = fillLine(originalLine, ' ', 39);
                    line = line + "O";
                }
            }
            return line;
        }

        /**
         * Fills a line to a given position with a given character.
         *
         * @param line the line to fill
         * @param c the fill character
         * @param pos the position to where to fill the line
         * @return the filled line
         */
        public static String fillLine(String line, char c, int pos) {
            StringBuffer sb = new StringBuffer(line);
            int positions = pos - line.length();
            if (positions > 0) {
                for (int i=0; i <positions; i++) {
                    sb.append(c);
                }
            }
            return sb.toString();
        }

    }
}
