/*
 * Created on 1-nov-2005
 *
 */
package be.foundation.utilities.as400.rpg2java.renderer;

import java.awt.Component;

import javax.swing.JCheckBox;
import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;

/**
 * @author    : Geert Van Landeghem
 * Copyright : Foundation.be
 */
public class SelectedCheckboxRenderer extends JCheckBox implements TableCellRenderer {

    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        setSelected((value != null) && (Boolean)value);
        return this;
    }



}
